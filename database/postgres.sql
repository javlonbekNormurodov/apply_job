-- Первая задача

create table groups
(
    id   int         not null primary key,
    name varchar(50) not null
);

create table users
(
    id                      int         not null primary key,
    group_id                int         not null,
    invited_by_user_id      int         not null,
    name                    varchar(50) not null,
    posts_qty               int         not null,
    constraint  fk_users_group_id
        foreign key (group_id) references `groups` (id)
            on update cascade on delete cascade
);

insert into groups 
    (id, name)
values
    (1, 'Группа 1'),
    (2, 'Группа 2');


insert into users
    (id, group_id, invited_by_user_id, name, posts_qty)
values
    (1, 1, 0, 'Пользователь 1', 0),
    (2, 1, 1, 'Пользователь 2', 2),
    (3, 1, 2, 'Пользователь 3', 5),
    (4, 2, 3, 'Пользователь 4', 7),
    (5, 2, 4, 'Пользователь 5', 1);

-- После того как я добавил все нужные мне таблицы и элементы, я снизу буду писать sql запросы

-- Выборка пользователей, у которых количество постов больше, чем у пользователя их пригласившего:
SELECT u1.id, u1.name
FROM users u1
INNER JOIN users u2 ON u1.invited_by_user_id = u2.id
WHERE u1.posts_qty > u2.posts_qty;


-- Выборка пользователей, имеющих максимальное количество постов в своей группе:
SELECT u.id, u.name
FROM users u
WHERE u.posts_qty = (SELECT MAX(posts_qty) FROM users WHERE group_id = u.group_id);


-- Выборка групп, количество пользователей в которых превышает 10000:
SELECT g.id, g.name
FROM groups g
WHERE (SELECT COUNT(*) FROM users WHERE group_id = g.id) > 10000;


-- Выборка пользователей, у которых пригласивший их пользователь из другой группы:
SELECT u.id, u.name
FROM users u
INNER JOIN users inviter ON u.invited_by_user_id = inviter.id
WHERE u.group_id <> inviter.group_id;


-- Выборка групп с максимальным количеством постов у пользователей:
SELECT g.id, g.name
FROM groups g
WHERE g.id = (SELECT group_id FROM users GROUP BY group_id ORDER BY SUM(posts_qty) DESC LIMIT 1);
