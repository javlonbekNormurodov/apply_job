-- SQL скрипт для отмены изменений
-- Удаление добавленных полей и восстановление типа поля age

-- Удаление добавленных полей
ALTER TABLE users
DROP COLUMN IF EXISTS last_name,
DROP COLUMN IF EXISTS age,
DROP COLUMN IF EXISTS created_at;

-- Восстановление исходного типа поля age
ALTER TABLE users
ALTER COLUMN age TYPE int;


-- SQL скрипт для отмены создания индексов

-- Удаление индекса на поле 'name'
DROP INDEX IF EXISTS indx_name;

-- Удаление составного индекса на полях 'id' и 'last_name'
DROP INDEX IF EXISTS indx_id_lastname;
