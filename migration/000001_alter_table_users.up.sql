-- Вторая Задача

-- Добавление трех новых полей (предположим, что таблица называется 'users')
ALTER TABLE users
ADD COLUMN last_name varchar(200),
ADD COLUMN age int,
ADD COLUMN created_at timestamp;


-- Изменение одного поля:
ALTER TABLE users
ALTER COLUMN age TYPE text;


-- Создание индекса на одном поле
CREATE INDEX indx_name ON users (name);

-- Создание составного индекса на двух полях
CREATE INDEX indx_id_lastname ON users (id, last_name);
