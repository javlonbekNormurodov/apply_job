package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/ClickHouse/clickhouse-go"
)

func main() {
	dsn := "tcp://clickhouse-server:9000?username=default&password=yourpassword&database=yourdb"

	// Открываем соединение с ClickHouse
	db, err := sql.Open("clickhouse", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Вставка тестовых данных
	_, err = db.Exec(`
		INSERT INTO events (eventID, eventType, userID, eventTime, payload)
		VALUES
			(1, 'type1', 1001, '2023-01-01 10:00:00', 'payload1'),
			(2, 'type2', 1002, '2023-02-05 12:30:00', 'payload2'),
			(3, 'type1', 1003, '2023-03-03 15:45:00', 'payload3');
	`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Данные успешно вставлены.")
}
