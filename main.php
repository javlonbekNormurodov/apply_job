

function countTuesdays($startDate, $endDate) {
    $startDate = new DateTime($startDate);
    $endDate = new DateTime($endDate);
    $interval = new DateInterval('P1D'); // Интервал в 1 день

    $tuesdayCount = 0;
    while ($startDate <= $endDate) {
        if ($startDate->format('N') == 2) { // 2 - это код для вторника (понедельник - 1, вторник - 2, и так далее)
            $tuesdayCount++;
        }
        $startDate->add($interval);
    }

    return $tuesdayCount;
}

// Пример использования:
$startDate = '2023-10-01'; // Начальная дата
$endDate = '2023-11-01';   // Конечная дата

$count = countTuesdays($startDate, $endDate);
echo "Количество вторников: $count";
