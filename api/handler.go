package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/ClickHouse/clickhouse-go"
	"test/model"
)

// Третье задание 

func main() {
	r := gin.Default()

	// Обработчик для POST-запросов на /api/event
	r.POST("/api/event", func(c *gin.Context) {
		// Принимаем JSON-данные о событии
		var event model.Event
		if err := c.BindJSON(&event); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Подключение к ClickHouse и вставка данных
		db, err := sqlx.Connect("clickhouse", "tcp://clickhouse-server:9000?username=default&password=yourpassword&database=yourdb")
		if err != nil {
			log.Fatal(err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to connect to ClickHouse"})
			return
		}
		defer db.Close()

		query := fmt.Sprintf(`
			INSERT INTO events (eventType, userID, eventTime, payload)
			VALUES ('%s', %d, '%s', '%s');
		`, event.EventType, event.UserID, event.EventTime, event.Payload)

		_, err = db.Exec(query)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to insert data into ClickHouse"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Event data inserted successfully"})
	})

	r.Run(":8080") // Запуск сервера на порту 8080
}
